package com.hcl.registeremployee.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.hcl.registeremployee.entity.Employee;

@Repository
public interface RegistrationRepository extends JpaRepository<Employee, Long> {

	Employee findByUserNameAndPassword(String userName, String Password);

}