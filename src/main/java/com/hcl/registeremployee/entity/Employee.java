package com.hcl.registeremployee.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
//@SecondaryTable(name = "Address", pkJoinColumns = @PrimaryKeyJoinColumn(name = "Address_Id"))
public class Employee {
	@Id
	@Column(name = "employeeId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long employeeId;
	@Column(name = "userName")
	private String userName;
	@Column(name = "password")
	private String password;
	@OneToOne(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL, mappedBy = "employeeInfo")
	private Address address;
	@OneToMany(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL, mappedBy = "employee")
	private List<ProductsEntity> products;
	@OneToMany(mappedBy = "employeeOrders")
	private List<Orders> orders;
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public List<ProductsEntity> getProducts() {
		return products;
	}
	public void setProducts(List<ProductsEntity> products) {
		this.products = products;
	}
	public List<Orders> getOrders() {
		return orders;
	}
	public void setOrders(List<Orders> orders) {
		this.orders = orders;
	}
	

}