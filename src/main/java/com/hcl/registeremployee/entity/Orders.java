package com.hcl.registeremployee.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Orders")
public class Orders {
	@Id
	@Column(name = "Order_Id")
	private Long orderId;
	@OneToOne(mappedBy = "orders")
	private ProductsEntity product;
	@ManyToOne
	@JoinColumn(name = "employeeId")
	private Employee employeeOrders;
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public ProductsEntity getProduct() {
		return product;
	}
	public void setProduct(ProductsEntity product) {
		this.product = product;
	}
	public Employee getEmployeeOrders() {
		return employeeOrders;
	}
	public void setEmployeeOrders(Employee employeeOrders) {
		this.employeeOrders = employeeOrders;
	}
	
}