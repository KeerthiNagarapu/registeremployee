package com.hcl.registeremployee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegisteremployeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegisteremployeeApplication.class, args);
	}

}
