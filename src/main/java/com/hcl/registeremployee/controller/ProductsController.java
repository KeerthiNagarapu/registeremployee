package com.hcl.registeremployee.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.registeremployee.entity.ProductsEntity;
import com.hcl.registeremployee.service.RegistrationService;

@RestController
@RequestMapping(value = "/product")
public class ProductsController {

	@Autowired
	RegistrationService productService;

	@PostMapping(value = "/add")
	public String addProduct(@RequestBody List<ProductsEntity> products) {
		productService.addProduct(products);
		return "Product added successfully";
	}

	@GetMapping(value = "/getProducts")
	public List<ProductsEntity> getProducts() {
		List<ProductsEntity> products = productService.getProducts();
		return products;
	}
}