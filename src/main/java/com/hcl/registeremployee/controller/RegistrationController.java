package com.hcl.registeremployee.controller;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.registeremployee.entity.Employee;
import com.hcl.registeremployee.service.RegistrationService;

@RestController
@RequestMapping("/value")
public class RegistrationController {

	@Autowired
	RegistrationService regiService;

	@RequestMapping("/register")
	public ResponseEntity register(@RequestBody Employee employee) {

		regiService.doRegistration(employee);
		System.out.println("/register/add");
		return ResponseEntity.ok().build();

	}

	private String genUniqueId() {
		UUID uuid = UUID.randomUUID();
		System.out.println(uuid.toString());
		return uuid.toString();
	}

	@RequestMapping("/login")
	public List<Employee> login(@RequestBody Employee employee) {
		/*
		 * List<Employee> employees = new ArrayList<>(); Employee em1 = new Employee();
		 * em1.setUserName("kheer"); em1.setPassword("panda"); employees.add(em1);
		 * List<Employee> specialEmployees = new ArrayList<>(); Employee em2 = new
		 * Employee(); em2.setUserName("panda"); em2.setPassword("kheer");
		 * specialEmployees.add(em2);
		 *
		 * employees.addAll(specialEmployees);
		 */

		System.out.println("login successful" + employee.getUserName());
		List<Employee> em = regiService.dologin(employee);
		if (!em.equals(null)) {
			System.out.println("there are records");
		} else {
			System.out.println("no records");
		}
		return em;

	}

}