package com.hcl.registeremployee.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.registeremployee.entity.Address;
import com.hcl.registeremployee.entity.Employee;
import com.hcl.registeremployee.entity.ProductsEntity;
import com.hcl.registeremployee.repository.AddressRepository;
import com.hcl.registeremployee.repository.ProductsRepository;
import com.hcl.registeremployee.repository.RegistrationRepository;

@Service
public class RegistrationService {

	@Autowired
	RegistrationRepository regiRepo;

	@Autowired
	ProductsRepository productRepo;

	@Autowired
	AddressRepository addRepo;

	public void doRegistration(Employee employee) {
		/*
		 * 
		 * List<Address> addList = new ArrayList<>(); for (Employee e : employee) {
		 * 
		 * System.out.println(e.getEmployeeId()); e.setAddress(e.getAddress()); Address
		 * 
		 * address = new Address(); address.setEmployeeInfo(e); addList.add(address); }
		 * 
		 */
		Employee employees = new Employee();
		employees.setUserName(employee.getUserName());
		employees.setAddress(employee.getAddress());
		Address add = employee.getAddress();
		// add.setCity(employee.getAddress().getCity());
		add.setEmployeeInfo(employee);
		regiRepo.save(employee);
		/*
		 * 
		 * List<Employee> employees = regiRepo.findAll(); for (Employee em : employees)
		 * 
		 * { addList.stream().forEach(x -> x.setEmployeeInfo(em)); }
		 * 
		 * addRepo.saveAll(addList);
		 * 
		 */
	}

	public List<Employee> dologin(Employee employee) {
		// Employee emp = regiRepo.findByUserNameAndPassword(employee.getUserName(),
		// employee.getPassword());
		List<Employee> employees = regiRepo.findAll();
		List<Employee> Kemployees = new ArrayList<>();
		for (Employee e : employees) {
			String name = e.getUserName();
			if (name.charAt(0) == 'k' || name.charAt(0) == 'K') {
				Kemployees.add(e);
			}
		}
		return Kemployees;
	}

	public void addProduct(List<ProductsEntity> products) {
		for (ProductsEntity pr : products) {
			System.out.println(pr.getProdDesc());
		}
		productRepo.saveAll(products);
	}

	public List<ProductsEntity> getProducts() {
		List<ProductsEntity> productsList = productRepo.findAll();
		return productsList;
	}
}